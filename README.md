# README #

### What is this repository for? ###

This is demo project to show how to create the REST service with using spring-boot

### How do I get set up? ###

To build the project execute:

```
    mvn clean install
```
DB setup not required, it will create h2 database files in the project home.

### REST api demonstration ###

   To create the new recrod:
   
   ```
   curl -i -X POST -H "Content-Type: application/json"
           -d '{"id": "test1",  "name": "Test one", "description": "Test one message" }'
           http://localhost:8080/topics
   ```
   
   To list the records:
   
   ```
   curl -i http://localhost:8080/topics
   ```