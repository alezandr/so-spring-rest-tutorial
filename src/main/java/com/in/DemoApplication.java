package com.in;

import org.h2.jdbcx.JdbcDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.SQLException;

@SpringBootApplication
public class DemoApplication {
//
//    @Autowired
//    public DataSource dataSource;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    //    @Bean
    //    public LocalSessionFactoryBean sessionFactory() {
    //        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
    //        sessionFactory.setDataSource(restDataSource());
    //        sessionFactory.setPackagesToScan(
    //                new String[] { "org.baeldung.spring.persistence.model" });
    //        sessionFactory.setHibernateProperties(hibernateProperties());
    //
    //        return sessionFactory;
    //    }

    @PostConstruct
    public void initDb() throws SQLException {
        dataSource().getConnection().createStatement().execute("CREATE TABLE topic (" +
                                                                       "id VARCHAR(200), " +
                                                                       "name VARCHAR(200), " +
                                                                       "description VARCHAR(200))");
    }

    @Bean
    DataSource dataSource() {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:./test");
        ds.setUser("sa");
        ds.setPassword("sa");
        return ds;
    }
}
