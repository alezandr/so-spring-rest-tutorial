package com.in.controller;

import com.in.entity.Topic;
import com.in.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TopicController {

    @Autowired
    private TopicService topicService;

    /**
     * Usage example:
     * <pre>
     * curl -i http://localhost:8080/topics
     * </pre>
     *
     * @return
     */
    @RequestMapping("/topics")
    public List<Topic> getAllTopics() {
        return topicService.getAllTopics();
    }

    @RequestMapping("/topics/{id}")
    public Topic getTopic(@PathVariable String id) {
        return topicService.getTopic(id);
    }

    /**
     *
     * Usage example:
     * <pre>
     * curl -i -X POST -H "Content-Type: application/json" \
     *      -d '{"id": "test1",  "name": "Test one", "description": "Test one message" }' \
     *      http://localhost:8080/topics
 *      </pre>
     *
     * @param topic
     */
    @PostMapping(value = "/topics")
    public void addTopic(@RequestBody Topic topic) {
        topicService.addTopic(topic);
    }

    @PutMapping(value = "topics/{id}")
    public void updateTopic(@RequestBody Topic topic, @PathVariable String id) {
        topicService.updateTopic(id, topic);
    }

    @DeleteMapping(value = "/topics/{id}")
    public void deleteTopic(@PathVariable String id) {
        topicService.deleteTopic(id);
    }
}